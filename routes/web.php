<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//home
Route::get('/home', [HomeController::class, 'index'])->name('index')->middleware('auth');

//auth
Route::get('/', [AuthController::class, 'index'])->name('index');
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register/post', [AuthController::class, 'registerPost'])->name('registerPost');
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login/post', [AuthController::class, 'loginPost'])->name('loginPost');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

//users
Route::get('/users/index', [UserController::class, 'index'])->name('users/index')->middleware('auth');
Route::get('/users/edit/{id}', [UserController::class, 'edit'])->name('users/edit')->middleware('auth');
Route::post('/users/edit/post', [UserController::class, 'editPost'])->name('users.editPost')->middleware('auth');
Route::get('/users/profile/{id}', [UserController::class, 'profileUser'])->name('users/profile')->middleware('auth');
Route::get('/users/profile/edit/{id}', [UserController::class, 'editProfile'])->name('users/profile/edit')->middleware('auth');
Route::post('/users/profile/edit/post', [UserController::class, 'editUser'])->name('users.editProfile')->middleware('auth');
