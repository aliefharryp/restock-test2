<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use DB;
use Session;

class AuthController extends Controller
{
    function register(){

        $contents = array();
        $contents['page_title'] = "Register User";
        $contents['page_name'] = "Register User";

        return view('auth.register', compact('contents'));

    }

    function registerPost(Request $request){
        try{
            $user = User::create([
                'username' => $request->uname,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
            return redirect('/login')->with('status', "Success Register User!");
        }catch(Throwable $e){
            return redirect('/register')->with('status', "Failed Register User!");
        }

    }

    function login(){

        $contents = array();
        $contents['page_title'] = "Login User";
        $contents['page_name'] = "Login User";

        return view('auth.login', compact('contents'));

    }

    function loginPost(Request $request){
        $data = [
            'username' => $request->uname,
            'password' => $request->password
        ];

        if (Auth::Attempt($data)){
            return redirect('/home');
        }else{
            return redirect('/login')->with('status', "Username/Email is wrong!");
        }
    }

    function logout(){
        Auth::logout();
        return redirect('login');
    }

    function index(){

        $contents = array();
        $contents['page_title'] = "Landing Page";
        $contents['page_name'] = "Landing Page";

        return view('index', compact('contents'));
    }
}
