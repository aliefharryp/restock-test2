<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    function index(){

        $contents = array();
        $contents['page_title'] = "Index";
        $contents['url_parent'] = "index";
        $contents['page_name'] = "Home Index";
        $contents['userid'] = Auth::getUser()->id;
        $contents['username'] = Auth::getUser()->username;

        return view('home.index', compact('contents'));
    }
}
