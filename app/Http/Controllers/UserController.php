<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    function index(){

        $data = DB::table('users')
                ->selectRaw("users.id, users.username, users.email, roles.name as rname, users.phone_number, case when users.status = 1 then 'ACTIVE' else 'INACTIVE' end as status, users.created_at, users.updated_at")
                ->join('roles', 'roles.id', 'users.roles_id')
                ->get();

        $contents = array();
        $contents['page_title'] = "Users";
        $contents['url_parent'] = "users";
        $contents['page_name'] = "Users Index";
        $contents['data'] = $data;

        return view('users.index', compact('contents'));
    }

    function edit($id){

        $data = DB::table('users')
                ->selectRaw("users.id, users.username, users.email, roles.name as rname, users.status, users.created_at, users.updated_at")
                ->join('roles', 'roles.id', 'users.roles_id')
                ->where('users.id', $id)
                ->first();
        
        $roles = DB::table('roles')->get();

        $status = [
            [
                'id' => 1,
                'name' => 'ACTIVE'
            ],
            [
                'id' => 2,
                'name' => 'INACTIVE'
            ]
        ];

        $contents = array();
        $contents['page_title'] = "Users";
        $contents['url_parent'] = "users";
        $contents['page_name'] = "Users Edit";
        $contents['data'] = $data;
        $contents['roles'] = $roles;
        $contents['status'] = $status;

        return view('users.edit', compact('contents'));

    }

    function editPost(Request $request){
        try{
            User::where('id', $request->cid)->update([
                "email" => $request->email,
                "status" => $request->status,
                "roles_id" => $request->roles,
            ]);
            return redirect('/users/index')->with('status', "Success Edit Data!");
        }catch(Throwable $e){
            return redirect('/users/edit')->with('status', "Failed Edit Data!");
        }
    }

    function profileUser($id){

        $data = DB::table('users')
                ->selectRaw("users.id, users.username, users.email, users.phone_number")
                ->where("users.id", $id)
                ->first();

        $contents = array();
        $contents['page_title'] = "Profile Users";
        $contents['url_parent'] = "users/profile";
        $contents['page_name'] = "Profile Users";
        $contents['data'] = $data;
        $contents['userid'] = Auth::getUser()->id;


        return view('users.profile', compact('contents'));
    }

    function editProfile($id){

        $data = DB::table('users')
                ->selectRaw("users.id, users.username, users.email, users.phone_number")
                ->where("users.id", $id)
                ->first();

        $contents = array();
        $contents['page_title'] = "Edit Profile Users";
        $contents['url_parent'] = "users/profile";
        $contents['page_name'] = "Edit Profile Users";
        $contents['data'] = $data;
        $contents['userid'] = Auth::getUser()->id;


        return view('users.editProfile', compact('contents'));
    }

    function editUser(Request $request){
        try{
            User::where('id', $request->uid)->update([
                "email" => $request->email,
                "phone_number" => $request->pnumber,
            ]);
            return redirect('/users/profile/'.$request->uid)->with('status', "Success Edit Data!");
        }catch(Throwable $e){
            return redirect('/users/profile/edit/'.$request->uid)->with('status', "Failed Edit Data!");
        }
    }
}
