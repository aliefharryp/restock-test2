<html>
    <head>
        <title> {{$contents['page_name']}} - Restock</title>
    </head>
    <style>
        table, th, td {
            border:1px solid black;
        }
    </style>
    <body>
       
        <div class="container">
            <div style="text-align:center">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <h1>Restock</h1>
                <table style="margin-left:auto; margin-right:auto;">
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>Roles</th>
                        <th>Status</th>
                        <th>Created Date</th>
                        <th>Update Date</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($contents['data'] as $data)
                    <tr>
                        <td>{{$data->username}}</td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->phone_number}}</td>
                        <td>{{$data->rname}}</td>
                        <td>{{$data->status}}</td>
                        <td>{{$data->created_at}}</td>
                        <td>{{$data->updated_at}}</td>
                        <td><a href="/{{$contents['url_parent']}}/edit/{{$data->id}}">Edit</a> |<a href="/{{$contents['url_parent']}}/delete/{{$data->id}}">Inactive</a></td>
                    </tr>
                    @endforeach
                </table><br><br>
            </div>
        </div>
    </body>
</html>
