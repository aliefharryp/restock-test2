<html>
    <head>
        <title> {{$contents['page_name']}} - Restock</title>
    </head>
    
    <body>
        <div class="container">
            <div style="text-align:center">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <h1>Restock</h1>
                <form action="{{route('users.editPost')}}" method="post">
                    @csrf
                    <input type="hidden" name="iid" value="{{$contents['data']->id}}">
                    <label for="username">Username</label><br>
                    <input type="text" name="username" id="username" value="{{$contents['data']->username}}" disabled><br><br>
                    <label for="email">Email</label><br>
                    <input type="text" name="email" id="email" value="{{$contents['data']->email}}"><br><br>
                    <label for="status">Status</label><br>
                    <select name="status">
                        @foreach($contents['status'] as $status)
                            @if ($contents['data']->status == $status['id'])
                                <option value="{{$status['id']}}" selected>{{$status['name']}}</option>
                            @else
                                <option value="{{$status['id']}}">{{$status['name']}}</option>
                            @endif
                        @endforeach
                    </select><br><br>
                    
                    <label for="roles">Roles</label><br>
                    <select name="roles">
                        @foreach($contents['roles'] as $roles)
                            @if ($contents['data']->id == $roles->id)
                                <option value="{{$roles->id}}" selected>{{$roles->name}}</option>
                            @else
                                <option value="{{$roles->id}}">{{$roles->name}}</option>
                            @endif
                        @endforeach
                    </select><br><br>
                    <input type="submit" value="Submit">
                </form>
            </div>
        </div>
    </body>
</html>
