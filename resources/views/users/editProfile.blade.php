<html>
    <head>
        <title> {{$contents['page_name']}} - Restock</title>
    </head>
    
    <body>
        <div class="container">
            <div style="text-align:center">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <h1>Restock</h1>
                <form action="{{route('users.editProfile')}}" method="post">
                    @csrf
                    <input type="hidden" name="uid" value="{{$contents['userid']}}">
                    <label for="username">Username</label><br>
                    <input type="text" name="username" id="username" value="{{$contents['data']->username}}" ><br><br>
                    <label for="email">Email</label><br>
                    <input type="text" name="email" id="email" value="{{$contents['data']->email}}" ><br><br>
                    <label for="status">Phone Number</label><br>
                    <input type="text" name="pnumber" id="pnumber" value="{{$contents['data']->phone_number}}" ><br><br>
                    <input type="submit" value="Submit">
                </form>
            </div>
        </div>
    </body>
</html>
