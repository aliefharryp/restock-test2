<html>
    <head>
        <title> {{$contents['page_name']}} - Restock</title>
    </head>
    
    <body>
        <div class="container">
            <div style="text-align:center">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <h1>Restock</h1>
                    <label for="username">Username</label><br>
                    <input type="text" name="username" id="username" value="{{$contents['data']->username}}" disabled><br><br>
                    <label for="email">Email</label><br>
                    <input type="text" name="email" id="email" value="{{$contents['data']->email}}" disabled><br><br>
                    <label for="status">Phone Number</label><br>
                    <input type="text" name="pnumber" id="pnumber" value="{{$contents['data']->phone_number}}" disabled><br><br>
                </form>
                <a href="/{{$contents['url_parent']}}/edit/{{$contents['userid']}}"><button type="button">Edit</button></a>
            </div>
        </div>
    </body>
</html>
