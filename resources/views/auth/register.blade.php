<html>
    <head>
        <title> {{$contents['page_name']}} - Restock</title>
    </head>
    
    <body>
        <div class="container">
            <div style="text-align:right">
                <a href="/login"><button type="button">Sign Up</button></a>
            </div>
            <div style="text-align:center">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{session('status')}}
                    </div>
                @endif
                <h1>Restock</h1>
                <form action="{{route('registerPost')}}" method="post">
                    @csrf
                    <label for="uname">Username</label><br>
                    <input type="text" name="uname" id="uname"><br><br>
                    <label for="email">Email</label><br>
                    <input type="text" name="email" id="email"><br><br>
                    <label for="password">Password</label><br>
                    <input type="password" name="password" id="password"><br><br>
                    <input type="submit" value="Submit">
                </form>
            </div>
        </div>
    </body>
</html>
